<?php

namespace App;



/**
 * App\BuyProperty
 *
 * @property mixed $id
 * @property string $title
 * @property int $owner_id
 * @property string $property_type
 * @property int $featured_property
 * @property string $property_category
 * @property string $property_status
 * @property string $address_1
 * @property string $address_2
 * @property int $project_id
 * @property string $accommodation
 * @property string $facing
 * @property float $total_sqft
 * @property string $furnished_status
 * @property int $balcony
 * @property string $overlooking
 * @property int $water_availability
 * @property int $power_backup
 * @property string|null $landmark
 * @property int $parking
 * @property float $maintenance_pmo
 * @property int|null $construction_age
 * @property int $rera_id
 * @property float $expected_price
 * @property int $registry_status
 * @property int $rental_value
 * @property string|null $description
 * @property int $exclusive_property
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereAccommodation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereAddress1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereBalcony($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereConstructionAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereExclusiveProperty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereExpectedPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereFacing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereFeaturedProperty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereFurnishedStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereLandmark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereMaintenancePmo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereOverlooking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereParking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty wherePowerBackup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty wherePropertyCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty wherePropertyStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty wherePropertyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereRegistryStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereRentalValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereReraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereTotalSqft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereWaterAvailability($value)
 * @mixin \Eloquent
 * @property int $exclusive
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BuyProperty whereExclusive($value)
 */
class BuyProperty extends BaseModel {


}
