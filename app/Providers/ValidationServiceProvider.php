<?php

namespace App\Providers;

use App\Services\RegexService;
use App\User;
use DateTime;
use Dingo\Api\Provider\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class ValidationServiceProvider extends ServiceProvider {
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Validator::extend('latitude', function ($attribute, $value) {
            return preg_match(RegexService::REGEX_LATITUDE, $value);
        });

        Validator::extend('longitude', function ($attribute, $value) {
            return preg_match(RegexService::REGEX_LONGITUDE, $value);
        });

        Validator::extend('web_url', function ($attribute, $value) {
            return preg_match(RegexService::REGEX_LINK, $value);
        });

        Validator::extend('date_string', function ($attribute, $value) {
            if ($value instanceof DateTime) {
                return true;
            }

            return !(strtotime($value) === false);
        });

        Validator::extend('alpha_spaces', function ($attribute, $value) {
            return preg_match(RegexService::REGEX_ALPHA_SPACES, $value);
        });

        Validator::extend('day_of_week', function ($attribute, $value) {
            $validTypes = [
                'sunday',
                'monday',
                'tuesday',
                'wednesday',
                'thursday',
                'friday',
                'saturday'
            ];

            return in_array($value, $validTypes);
        });
        Validator::extend('valid_user_types', function ($attribute, $value) {
            $validTypes = [
                'buyer',
                'seller'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_project_categories', function ($attribute, $value) {
            $validTypes = [
                'residential',
                'commercial'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_project_status', function ($attribute, $value) {
            $validTypes = [
                'rtm',
                'uc'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_property_types', function ($attribute, $value) {
            $validTypes = [
                'residential',
                'commercial'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_rent_types', function ($attribute, $value) {
            $validTypes = [
                'rent',
                'pg'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_property_categories', function ($attribute, $value) {
            $validTypes = [
                'apartment',
                'flat',
                'builder_floor',
                'land',
                'house',
                'villa'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_accommodation_types', function ($attribute, $value) {
            $validTypes = [
                'one',
                'one_study',
                'two',
                'two_study',
                'three',
                'three_study',
                'four',
                'four_study'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_facing_types', function ($attribute, $value) {
            $validTypes = [
                'north',
                'south',
                'east',
                'west'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_furnished_types', function ($attribute, $value) {
            $validTypes = [
                'fully_furnished',
                'semi_furnished',
                'unfurnished'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_overlooking_types', function ($attribute, $value) {
            $validTypes = [
                'park',
                'road',
                'other'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_availability_types', function ($attribute, $value) {
            $validTypes = [
                'immediate',
                'specific_type'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_tenant_types', function ($attribute, $value) {
            $validTypes = [
                'family',
                'bachelor',
                'any'
            ];

            return in_array($value, $validTypes);
        });

        Validator::extend('valid_kc_category_types', function ($attribute, $value) {
            $validTypes = [
                'indian',
                'nri'
            ];

            return in_array($value, $validTypes);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }
}
