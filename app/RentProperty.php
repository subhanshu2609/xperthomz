<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\RentProperty
 *
 * @property int $id
 * @property string $title
 * @property int $owner_id
 * @property string $property_type
 * @property string $rent_type
 * @property int $featured_property
 * @property string $property_category
 * @property string $address_1
 * @property string $address_2
 * @property string $accommodation
 * @property string $facing
 * @property float $total_sqft
 * @property string $furnished_status
 * @property int $balcony
 * @property string $overlooking
 * @property int $water
 * @property int $power_backup
 * @property string|null $landmark
 * @property int $parking
 * @property float $maintenance_pmo
 * @property int|null $construction_age
 * @property float $expected_rent
 * @property string $availability
 * @property string|null $tenant_type
 * @property string|null $description
 * @property int $exclusive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereAccommodation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereAddress1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereAvailability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereBalcony($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereConstructionAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereExclusive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereExpectedRent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereFacing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereFeaturedProperty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereFurnishedStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereLandmark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereMaintenancePmo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereOverlooking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereParking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty wherePowerBackup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty wherePropertyCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty wherePropertyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereRentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereTenantType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereTotalSqft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereWater($value)
 * @mixin \Eloquent
 * @property int $water_availability
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentProperty whereWaterAvailability($value)
 */
class RentProperty extends BaseModel
{
    const COMMERCIAL = 'commercial';
    const RESIDENTIAL = 'residential';

    const RENT = 'rent';
    const PG = 'pg';

    public function users() {
        return $this->belongsTo(User::class);
    }
}
