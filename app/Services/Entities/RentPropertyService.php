<?php


namespace App\Services\Entities;


use App\RentProperty;
use App\Services\Contracts\RentPropertyCreateContract;
use App\Services\Contracts\RentPropertyUpdateContract;
use Aws\Api\Service;

class RentPropertyService
{

    public function create($userId, RentPropertyCreateContract $contract)
    {
        $rentProperty = new RentProperty();

        $rentProperty->title = $contract->getTitle();
        $rentProperty->owner_id = $userId;
        $rentProperty->property_type = $contract->getPropertyType();
        $rentProperty->rent_type = $contract->getRentType();
        $rentProperty->featured_property = $contract->getFeaturedProperty();
        $rentProperty->property_category = $contract->getPropertyCategory();
        $rentProperty->address_1 = $contract->getAddress1();
        $rentProperty->address_2 = $contract->getAddress2();
        $rentProperty->accommodation = $contract->getAccommodation();
        $rentProperty->facing = $contract->getFacing();
        $rentProperty->total_sqft = $contract->getTotalSqft();
        $rentProperty->furnished_status = $contract->getFurnishedStatus();
        $rentProperty->balcony = $contract->getBalcony();
        $rentProperty->overlooking = $contract->getOverlooking();
        $rentProperty->water_availability = $contract->getWaterAvailability();
        $rentProperty->power_backup = $contract->getPowerBackup();

        if ($contract->hasLandmark() && !is_null($contract->getLandmark())) {
            $rentProperty->landmark = $contract->getLandmark();
        }

        $rentProperty->parking = $contract->getParking();
        $rentProperty->maintenance_pmo = $contract->getMaintenancePmo();

        if ($contract->hasConstructionAge() && !is_null($contract->getConstructionAge())) {
            $rentProperty->construction_age = $contract->getConstructionAge();
        }

        $rentProperty->expected_rent = $contract->getExpectedRent();
        $rentProperty->availability = $contract->getAvailability();
        $rentProperty->tenant_type = $contract->getTenantType();

        if ($contract->hasDescription() && !is_null($contract->getDescription())) {
            $rentProperty->description = $contract->getDescription();
        }

        $rentProperty->exclusive = $contract->getExclusive();

        $rentProperty->save();
        return $rentProperty;
    }

    public function update(RentPropertyUpdateContract $contract, RentProperty $rentProperty) {

        $isDirty = false;

        if ($contract->hasTitle()) {
            $isDirty = true;
            $rentProperty->title = $contract->getTitle();
        }
        if ($contract->hasPropertyType()) {
            $isDirty = true;
            $rentProperty->property_type = $contract->getPropertyType();
        }
        if ($contract->hasRentType()) {
            $isDirty = true;
            $rentProperty->rent_type = $contract->getRentType();
        }
        if ($contract->hasFeaturedProperty()) {
            $isDirty = true;
            $rentProperty->featured_property = $contract->getFeaturedProperty();
        }
        if ($contract->hasPropertyCategory()) {
            $isDirty = true;
            $rentProperty->property_category = $contract->getPropertyCategory();
        }
        if ($contract->hasAddress1()) {
            $isDirty = true;
            $rentProperty->address_1 = $contract->getAddress1();
        }
        if ($contract->hasAddress2()) {
            $isDirty = true;
            $rentProperty->address_2 = $contract->getAddress2();
        }
        if ($contract->hasAccommodation()) {
            $isDirty = true;
            $rentProperty->accommodation = $contract->getAccommodation();
        }
        if ($contract->hasFacing()) {
            $isDirty = true;
            $rentProperty->facing = $contract->getFacing();
        }
        if ($contract->hasTotalSqft()) {
            $isDirty = true;
            $rentProperty->total_sqft = $contract->getTotalSqft();
        }
        if ($contract->hasFurnishedStatus()) {
            $isDirty = true;
            $rentProperty->furnished_status = $contract->getFurnishedStatus();
        }
        if ($contract->hasBalcony()) {
            $isDirty = true;
            $rentProperty->balcony = $contract->getBalcony();
        }
        if ($contract->hasOverlooking()) {
            $isDirty = true;
            $rentProperty->overlooking = $contract->getOverlooking();
        }
        if ($contract->hasWaterAvailability()) {
            $isDirty = true;
            $rentProperty->water_availability = $contract->getWaterAvailability();
        }
        if ($contract->hasPowerBackup()) {
            $isDirty = true;
            $rentProperty->power_backup = $contract->getPowerBackup();
        }
        if ($contract->hasParking()) {
            $isDirty = true;
            $rentProperty->parking = $contract->getParking();
        }
        if ($contract->hasMaintenancePmo()) {
            $isDirty = true;
            $rentProperty->maintenance_pmo = $contract->getMaintenancePmo();
        }
        if ($contract->hasConstructionAge()) {
            $isDirty = true;
            $rentProperty->construction_age = $contract->getConstructionAge();
        }
        if ($contract->hasExpectedRent()) {
            $isDirty = true;
            $rentProperty->expected_rent = $contract->getExpectedRent();
        }
        if ($contract->hasAvailability()) {
            $isDirty = true;
            $rentProperty->availability = $contract->getAvailability();
        }
        if ($contract->hasTenantType()) {
            $isDirty = true;
            $rentProperty->tenant_type = $contract->getTenantType();
        }
        if ($contract->hasDescription()) {
            $isDirty = true;
            $rentProperty->description = $contract->getDescription();
        }
        if ($contract->hasExclusive()) {
            $isDirty = true;
            $rentProperty->exclusive = $contract->getExclusive();
        }

        if ($isDirty) {
            $rentProperty->save();
        }
        return $rentProperty;
    }

    public function show($rentPropertyId) {
        return RentProperty::find($rentPropertyId);
    }

    public function index() {
        return RentProperty::all();
    }

    public function delete(RentProperty $rentProperty) {
        $rentProperty->delete();
    }
}