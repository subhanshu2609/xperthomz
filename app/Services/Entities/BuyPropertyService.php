<?php


namespace App\Services\Entities;


use App\BuyProperty;
use App\Services\Contracts\BuyPropertyCreateContract;
use App\Services\Contracts\BuyPropertyUpdateContract;

class BuyPropertyService
{

    public function create($userId, BuyPropertyCreateContract $contract)
    {
        $buyProperty = new BuyProperty();

        $buyProperty->title = $contract->getTitle();
        $buyProperty->owner_id = $userId;
        $buyProperty->property_type = $contract->getPropertyType();
        $buyProperty->featured_property = $contract->getFeaturedProperty();
        $buyProperty->property_category = $contract->getPropertyCategory();
        $buyProperty->property_status = $contract->getPropertyStatus();
        $buyProperty->address_1 = $contract->getAddress1();
        $buyProperty->address_2 = $contract->getAddress2();
        $buyProperty->project_id = $contract->getProjectId();
        $buyProperty->accommodation = $contract->getAccommodation();
        $buyProperty->facing = $contract->getFacing();
        $buyProperty->total_sqft = $contract->getTotalSqft();
        $buyProperty->furnished_status = $contract->getFurnishedStatus();
        $buyProperty->balcony = $contract->getBalcony();
        $buyProperty->overlooking = $contract->getOverlooking();
        $buyProperty->water_availability = $contract->getWaterAvailability();
        $buyProperty->power_backup = $contract->getPowerBackup();

        if ($contract->hasLandmark() && !is_null($contract->getLandmark())) {
            $buyProperty->landmark = $contract->getLandmark();
        }

        $buyProperty->parking = $contract->getParking();
        $buyProperty->maintenance_pmo = $contract->getMaintenancePmo();

        if ($contract->hasConstructionAge() && !is_null($contract->getConstructionAge())) {
            $buyProperty->construction_age = $contract->getConstructionAge();
        }

        $buyProperty->rera_id = $contract->getReraId();
        $buyProperty->expected_price = $contract->getExpectedPrice();
        $buyProperty->registry_status = $contract->getRegistryStatus();
        $buyProperty->rental_value = $contract->getRentalValue();

        if ($contract->hasDescription() && !is_null($contract->getDescription())) {
            $buyProperty->description = $contract->getDescription();
        }

        $buyProperty->exclusive = $contract->getExclusive();

        $buyProperty->save();
        return $buyProperty;
    }

    public function update(BuyPropertyUpdateContract $contract, BuyProperty $buyProperty) {

        $isDirty = false;

        if ($contract->hasTitle()) {
            $isDirty = true;
            $buyProperty->title = $contract->getTitle();
        }
        if ($contract->hasPropertyType()) {
            $isDirty = true;
            $buyProperty->property_type = $contract->getPropertyType();
        }
        if ($contract->hasPropertyStatus()) {
            $isDirty = true;
            $buyProperty->property_status = $contract->getPropertyStatus();
        }
        if ($contract->hasFeaturedProperty()) {
            $isDirty = true;
            $buyProperty->featured_property = $contract->getFeaturedProperty();
        }
        if ($contract->hasPropertyCategory()) {
            $isDirty = true;
            $buyProperty->property_category = $contract->getPropertyCategory();
        }
        if ($contract->hasAddress1()) {
            $isDirty = true;
            $buyProperty->address_1 = $contract->getAddress1();
        }
        if ($contract->hasAddress2()) {
            $isDirty = true;
            $buyProperty->address_2 = $contract->getAddress2();
        }
        if ($contract->hasAccommodation()) {
            $isDirty = true;
            $buyProperty->accommodation = $contract->getAccommodation();
        }
        if ($contract->hasFacing()) {
            $isDirty = true;
            $buyProperty->facing = $contract->getFacing();
        }
        if ($contract->hasTotalSqft()) {
            $isDirty = true;
            $buyProperty->total_sqft = $contract->getTotalSqft();
        }
        if ($contract->hasFurnishedStatus()) {
            $isDirty = true;
            $buyProperty->furnished_status = $contract->getFurnishedStatus();
        }
        if ($contract->hasBalcony()) {
            $isDirty = true;
            $buyProperty->balcony = $contract->getBalcony();
        }
        if ($contract->hasOverlooking()) {
            $isDirty = true;
            $buyProperty->overlooking = $contract->getOverlooking();
        }
        if ($contract->hasWaterAvailability()) {
            $isDirty = true;
            $buyProperty->water_availability = $contract->getWaterAvailability();
        }
        if ($contract->hasPowerBackup()) {
            $isDirty = true;
            $buyProperty->power_backup = $contract->getPowerBackup();
        }
        if ($contract->hasParking()) {
            $isDirty = true;
            $buyProperty->parking = $contract->getParking();
        }
        if ($contract->hasMaintenancePmo()) {
            $isDirty = true;
            $buyProperty->maintenance_pmo = $contract->getMaintenancePmo();
        }
        if ($contract->hasConstructionAge()) {
            $isDirty = true;
            $buyProperty->construction_age = $contract->getConstructionAge();
        }
        if ($contract->hasReraId()) {
            $isDirty = true;
            $buyProperty->rera_id = $contract->getReraId();
        }
        if ($contract->hasExpectedPrice()) {
            $isDirty = true;
            $buyProperty->expected_price = $contract->getExpectedPrice();
        }
        if ($contract->hasRegistryStatus()) {
            $isDirty = true;
            $buyProperty->registry_status = $contract->getRegistryStatus();
        }
        if ($contract->hasRentalValue()) {
            $isDirty = true;
            $buyProperty->rental_value = $contract->getRentalValue();
        }
        if ($contract->hasDescription()) {
            $isDirty = true;
            $buyProperty->description = $contract->getDescription();
        }
        if ($contract->hasExclusive()) {
            $isDirty = true;
            $buyProperty->exclusive = $contract->getExclusive();
        }

        if ($isDirty) {
            $buyProperty->save();
        }
        return $buyProperty;
    }

    public function show($buyPropertyId) {
        return BuyProperty::find($buyPropertyId);
    }

    public function index() {
        return BuyProperty::all();
    }

    public function delete(BuyProperty $buyProperty) {
        $buyProperty->delete();
    }
}