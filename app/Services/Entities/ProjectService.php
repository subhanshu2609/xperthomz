<?php


namespace App\Services\Entities;


use App\Project;
use App\ProjectAccommodation;
use App\ProjectCommonFeatures;
use App\ProjectDetails;
use App\Services\Contracts\ProjectCreateContract;
use App\Services\Contracts\ProjectUpdateContract;

class ProjectService
{
    public function create(ProjectCreateContract $contract)
    {
        $project = new Project();
        $projectAccommodation = new ProjectAccommodation();
        $projectCommonFeatures = new ProjectCommonFeatures();
        $projectDetails = new ProjectDetails();

        $project->name = $contract->getName();
        $project->category = $contract->getCategory();
        $project->area = $contract->getArea();
        $project->address = $contract->getAddress();
        $project->description = $contract->hasDescription();
        $project->description = $contract->getDescription();
        $project->status = $contract->getStatus();

        $project->save();

        $projectAccommodation->project_id = $project->id;
        $projectAccommodation->type = $contract->getType();
        $projectAccommodation->price_per_sqft = $contract->getPricePerSqft();
        $projectAccommodation->total_sqft = $contract->getTotalSqft();
        $projectAccommodation->furnished_status = $contract->getFurnishedStatus();

        $projectAccommodation->save();

        $projectCommonFeatures->project_id = $project->id;
        $projectCommonFeatures->feature_name = $contract->getFeatureName();

        $projectCommonFeatures->save();

        $projectDetails->project_id  = $project->id;
        $projectDetails->total_flats = $contract->getTotalFlats();
        $projectDetails->tower_count = $contract->getTowerCount();
        $projectDetails->price_range = $contract->getPriceRange();
        $projectDetails->size_range  = $contract->getSizeRange();

        $projectDetails->save();

        return $project;
    }

    public function update(Project $project, ProjectUpdateContract $contract)
    {
       $isProject = false;
       $isAccommodation = false;
       $isCommonFeatures = false;
       $isDetails = false;

       $projectAccommodation = ProjectAccommodation::where('id', $project->id)->first();
       $projectCommonFeatures = ProjectCommonFeatures::where('id', $project->id);
       $projectDetails = ProjectDetails::where('id', $project->id);

       if($contract->hasName()){
           $isProject = true;
           $project->name = $contract->getName();
       }

       if ($contract->hasCategory()){
           $isProject = true;
           $project->category = $contract->getCategory();
       }

        if ($contract->hasArea()){
            $isProject = true;
            $project->area = $contract->getArea();
        }

        if ($contract->hasAddress()){
            $isProject = true;
            $project->address = $contract->getAddress();
        }

        if ($contract->hasDescription()){
            $isProject = true;
            $project->description =  $contract->hasDescription();
        }

        if ($contract->hasStatus()){
            $isProject = true;
            $project->status = $contract->getStatus();
        }

        if ($contract->hasType()){
            $isAccommodation = true;
            $projectAccommodation->type = $contract->getType();
        }

        if ($contract->hasPricePerSqft()){
            $isAccommodation = true;
            $projectAccommodation->price_per_sqft = $contract->getPricePerSqft();
        }

        if ($contract->hasTotalSqft()){
            $isAccommodation = true;
            $projectAccommodation->total_sqft = $contract->getTotalSqft();
        }

        if ($contract->hasFurnishedStatus()){
            $isAccommodation = true;
            $projectAccommodation->furnished_status = $contract->getFurnishedStatus();
        }

        if ($contract->hasFeatureName()){
            $isCommonFeatures = true;
            $projectCommonFeatures->feature_name = $contract->getFeatureName();
        }

        if ($contract->hasTotalFlats()){
            $isDetails = true;
            $projectDetails->total_flats = $contract->getTotalFlats();
        }

        if ($contract->hasTowerCount()){
            $isDetails = true;
            $projectDetails->tower_count = $contract->getTowerCount();
        }

        if ($contract->hasPriceRange()){
            $isDetails = true;
            $projectDetails->price_range = $contract->getPriceRange();
        }

        if ($contract->hasSizeRange()){
            $isDetails = true;
            $projectDetails->size_range = $contract->getSizeRange();
        }

        if($isProject) {
            $project->save();
        }

        if($isAccommodation) {
            $projectAccommodation->save();
        }

        return $project;

    }

    public function show($projectId) {
        return Project::find($projectId);
    }

    public function index() {
        $projects =Project::all();
        return $projects;
    }

    public function delete(Project $project)
    {
        $project->delete();
        return "success";
    }

}
