<?php


namespace App\Services\Entities;


use App\Service;
use App\Services\Contracts\ServiceCreateContract;
use App\Services\Contracts\ServiceUpdateContract;

class ServiceService
{
    public function create(ServiceCreateContract $contract) {
        $service = new Service();

        $service->heading = $contract->getHeading();
        $service->description = $contract->getDescription();

        $service->save();
        return $service;
    }

    public function update(Service $service, ServiceUpdateContract $contract)
    {
        $isDirty = false;

        if ($contract->hasHeading()) {
            $isDirty = true;
            $service->heading = $contract->getHeading();
        }
        if ($contract->hasDescription()) {
            $isDirty = true;
            $service->description = $contract->getDescription();
        }

        if ($isDirty) {
            $service->save();
        }

        return $service;
    }

    public function show($serviceId) {
        return Service::find($serviceId);
    }

    public function index() {
        $services = Service::all();
        return $services;
    }

    public function delete(Service $service) {
        $service->delete();
        return "success";
    }
}
