<?php

namespace App\Services\Entities;


use App\Services\Contracts\UserCreateContract;
use App\Services\Contracts\UserUpdateContract;
use App\User;

class UserService {
    public function showUserById($userId) {
        return User::find($userId);
    }

    public function index() {
        return User::whereNotIn('type', [User::TYPE_OWNER])->get();
    }

    public function create(UserCreateContract $contract) {
        $user = new User();

        $user->first_name = $contract->getFirstName();

        if ($contract->hasLastName()) {
            $user->last_name = $contract->getLastName();
        }

        $user->phone_number = $contract->getPhoneNumber();
        $user->email = $contract->getEmail();
        $user->type = $contract->getType();
        $user->password = $contract->getPassword();

        $user->save();
        return $user;
    }

    public function update(User $user, UserUpdateContract $contract) {
        $isDirty = false;

        if ($contract->hasFirstName()) {
            $isDirty    = true;
            $user->first_name = $contract->getFirstName();
        }
        if ($contract->hasLastName()) {
            $isDirty    = true;
            $user->last_name = $contract->getLastName();
        }
        if ($contract->hasPhoneNumber()) {
            $isDirty    = true;
            $user->phone_number = $contract->getPhoneNumber();
        }
        if ($contract->hasEmail()) {
            $isDirty    = true;
            $user->email = $contract->getEmail();
        }
        if ($contract->hasType()) {
            $isDirty    = true;
            $user->type = $contract->getType();
        }
        if ($contract->hasPassword()) {
            $isDirty    = true;
            $user->password = $contract->getPassword();
        }

        if ($isDirty) {
            $user->save();
        }

        return $user;
    }

    public function delete(User $user) {
        $user->delete();
    }

}
