<?php


namespace App\Services\Entities;


use App\KnowledgeCenter;
use App\Services\Contracts\KnowledgeCenterCreateContract;
use App\Services\Contracts\KnowledgeCenterUpdateContract;

class KnowledgeCenterService {

    public function index() {
        return KnowledgeCenter::all();
    }

    public function create(KnowledgeCenterCreateContract $contract) {
        $knowledgeCenter = new KnowledgeCenter();

        $knowledgeCenter->category = $contract->getCategory();
        $knowledgeCenter->headline = $contract->getHeadline();
        $knowledgeCenter->description = $contract->getDescription();

        $knowledgeCenter->save();
        return $knowledgeCenter;
    }

    public function update(KnowledgeCenter $knowledgeCenter, KnowledgeCenterUpdateContract $contract) {
        $isClear = false;

        if ($contract->hasCategory()) {
            $isClear = true;
            $knowledgeCenter->category = $contract->getCategory();
        }

        if ($contract->hasHeadline()) {
            $isClear = true;
            $knowledgeCenter->headline = $contract->getHeadline();
        }

        if ($contract->hasDescription()) {
            $isClear = true;
            $knowledgeCenter->description = $contract->getDescription();
        }

        if ($isClear) {
            $knowledgeCenter->save();
        }

        return $knowledgeCenter;
    }

    public function show($knowledgeCenterId) {
        return KnowledgeCenter::find($knowledgeCenterId);
    }

    public function delete(KnowledgeCenter $knowledgeCenter) {

        $knowledgeCenter->delete();
        return "success";
    }
}
