<?php


namespace App\Services\Contracts;


interface KnowledgeCenterCreateContract {
    public function getCategory();

    public function getHeadline();

    public function getDescription();

}
