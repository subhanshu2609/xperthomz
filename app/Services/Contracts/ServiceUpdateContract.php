<?php


namespace App\Services\Contracts;


interface ServiceUpdateContract{

    public function getHeading();

    public function hasHeading();

    public function getDescription();

    public function hasDescription();

}
