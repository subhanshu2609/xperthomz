<?php


namespace App\Services\Contracts;


interface RentPropertyUpdateContract {

    public function hasTitle();
    public function hasPropertyType();
    public function hasRentType();
    public function hasFeaturedProperty();
    public function hasPropertyCategory();
    public function hasAddress1();
    public function hasAddress2();
    public function hasAccommodation();
    public function hasFacing();
    public function hasTotalSqft();
    public function hasFurnishedStatus();
    public function hasBalcony();
    public function hasOverlooking();
    public function hasWaterAvailability();
    public function hasPowerBackup();
    public function hasLandmark();
    public function hasParking();
    public function hasMaintenancePmo();
    public function hasConstructionAge();
    public function hasExpectedRent();
    public function hasAvailability();
    public function hasTenantType();
    public function hasDescription();
    public function hasExclusive();
    public function getTitle();
    public function getPropertyType();
    public function getRentType();
    public function getFeaturedProperty();
    public function getPropertyCategory();
    public function getAddress1();
    public function getAddress2();
    public function getAccommodation();
    public function getFacing();
    public function getTotalSqft();
    public function getFurnishedStatus();
    public function getBalcony();
    public function getOverlooking();
    public function getWaterAvailability();
    public function getPowerBackup();
    public function getLandmark();
    public function getParking();
    public function getMaintenancePmo();
    public function getConstructionAge();
    public function getExpectedRent();
    public function getAvailability();
    public function getTenantType();
    public function getDescription();
    public function getExclusive();
}