<?php


namespace App\Services\Contracts;


interface BuyPropertyCreateContract
{
    public function hasLandmark();
    public function hasConstructionAge();
    public function hasDescription();
    public function getTitle();
    public function getPropertyType();
    public function getFeaturedProperty();
    public function getPropertyCategory();
    public function getPropertyStatus();
    public function getAddress1();
    public function getAddress2();
    public function getProjectId();
    public function getAccommodation();
    public function getFacing();
    public function getTotalSqft();
    public function getFurnishedStatus();
    public function getBalcony();
    public function getOverlooking();
    public function getWaterAvailability();
    public function getPowerBackup();
    public function getLandmark();
    public function getParking();
    public function getMaintenancePmo();
    public function getConstructionAge();
    public function getReraId();
    public function getExpectedPrice();
    public function getRegistryStatus();
    public function getRentalValue();
    public function getDescription();
    public function getExclusive();
}