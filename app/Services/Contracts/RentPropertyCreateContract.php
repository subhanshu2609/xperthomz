<?php


namespace App\Services\Contracts;


interface RentPropertyCreateContract {

    public function hasLandmark();
    public function hasConstructionAge();
    public function hasDescription();
    public function getTitle();
    public function getPropertyType();
    public function getRentType();
    public function getFeaturedProperty();
    public function getPropertyCategory();
    public function getAddress1();
    public function getAddress2();
    public function getAccommodation();
    public function getFacing();
    public function getTotalSqft();
    public function getFurnishedStatus();
    public function getBalcony();
    public function getOverlooking();
    public function getWaterAvailability();
    public function getPowerBackup();
    public function getLandmark();
    public function getParking();
    public function getMaintenancePmo();
    public function getConstructionAge();
    public function getExpectedRent();
    public function getAvailability();
    public function getTenantType();
    public function getDescription();
    public function getExclusive();
}