<?php


namespace App\Services\Contracts;


interface ProjectUpdateContract
{
    public function hasName();

    public function hasCategory();

    public function hasArea();

    public function hasAddress();

    public function hasDescription();

    public function hasStatus();

    public function hasType();

    public function hasPricePerSqft();

    public function hasTotalSqft();

    public function hasFurnishedStatus();

    public function hasFeatureName();

    public function hasTotalFlats();

    public function hasTowerCount();

    public function hasPriceRange();

    public function hasSizeRange();

    public function getName();

    public function getCategory();

    public function getArea();

    public function getAddress();

    public function getDescription();

    public function getStatus();

    public function getType();

    public function getPricePerSqft();

    public function getTotalSqft();

    public function getFurnishedStatus();

    public function getFeatureName();

    public function getTotalFlats();

    public function getTowerCount();

    public function getPriceRange();

    public function getSizeRange();


}
