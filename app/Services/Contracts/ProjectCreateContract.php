<?php


namespace App\Services\Contracts;


interface ProjectCreateContract {

    public function getName();

    public function getCategory();

    public function getArea();

    public function getAddress();

    public function hasDescription();

    public function getDescription();

    public function getStatus();

    public function getType();

    public function getPricePerSqft();

    public function getTotalSqft();

    public function getFurnishedStatus();

    public function getFeatureName();

    public function getTotalFlats();

    public function getTowerCount();

    public function getPriceRange();

    public function getSizeRange();
}
