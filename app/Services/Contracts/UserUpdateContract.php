<?php


namespace App\Services\Contracts;


interface UserUpdateContract {
    public function hasFirstName();

    public function hasLastName();

    public function hasEmail();

    public function hasPhoneNumber();

    public function hasType();

    public function hasPassword();

    public function getFirstName();

    public function getLastName();

    public function getEmail();

    public function getPhoneNumber();

    public function getType();

    public function getPassword();
}
