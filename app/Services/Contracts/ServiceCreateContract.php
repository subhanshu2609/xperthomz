<?php


namespace App\Services\Contracts;


interface ServiceCreateContract {
    public function getHeading();

    public function getDescription();
}
