<?php


namespace App\Services\Contracts;


interface KnowledgeCenterUpdateContract
{
    public function hasCategory();

    public function hasHeadline();

    public function hasDescription();

    public function getCategory();

    public function getHeadline();

    public function getDescription();
}
