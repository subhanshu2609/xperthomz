<?php


namespace App\Services\Contracts;


interface UserCreateContract {
    public function hasLastName();

    public function getFirstName();

    public function getLastName();

    public function getEmail();

    public function getPhoneNumber();

    public function getType();

    public function getPassword();
}