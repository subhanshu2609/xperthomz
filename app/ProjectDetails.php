<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProjectDetails
 *
 * @property mixed $id
 * @property int $project_id
 * @property int $total_flats
 * @property int $tower_count
 * @property string $price_range
 * @property string $size_range
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectDetails newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectDetails newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectDetails query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectDetails whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectDetails wherePriceRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectDetails whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectDetails whereSizeRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectDetails whereTotalFlats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectDetails whereTowerCount($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectDetails whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectDetails whereUpdatedAt($value)
 */
class ProjectDetails extends BaseModel
{
    //
}
