<?php

namespace App;


/**
 * App\KnowledgeCenter
 *
 * @property mixed $id
 * @property string $category
 * @property string $headline
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnowledgeCenter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnowledgeCenter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnowledgeCenter query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnowledgeCenter whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnowledgeCenter whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnowledgeCenter whereHeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnowledgeCenter whereId($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnowledgeCenter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\KnowledgeCenter whereUpdatedAt($value)
 */
class KnowledgeCenter extends BaseModel {
    //categories in knowledge center
    const INDIAN = 'indian';
    const NRI    = 'nri';
}
