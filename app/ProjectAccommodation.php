<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProjectAccommodation
 *
 * @property mixed $id
 * @property int $project_id
 * @property string $type
 * @property int $price_per_sqft
 * @property int $total_sqft
 * @property string $furnished_status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectAccommodation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectAccommodation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectAccommodation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectAccommodation whereFurnishedStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectAccommodation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectAccommodation wherePricePerSqft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectAccommodation whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectAccommodation whereTotalSqft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectAccommodation whereType($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectAccommodation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectAccommodation whereUpdatedAt($value)
 */
class ProjectAccommodation extends BaseModel
{
    const UNFURNISHED = 'unfurnished';
    const SEMI_FURNISHED = 'semi_furnished';
    const FULLY_FURNISHED = 'fully_furnished';

}
