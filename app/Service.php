<?php

namespace App;

use App\Services\Contracts\ServiceCreateContract;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Service
 *
 * @property mixed $id
 * @property string $heading
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereHeading($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereId($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereUpdatedAt($value)
 */
class Service extends BaseModel
{

}
