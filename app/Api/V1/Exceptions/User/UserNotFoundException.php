<?php

namespace App\Api\V1\Exceptions\User;


use App\Api\V1\Exceptions\ApiErrorCode;
use App\Api\V1\Exceptions\ModelNotFoundException;

class UserNotFoundException extends ModelNotFoundException {
    const ERROR_MESSAGE = "Requested User Does not exist.";

    public function __construct() {
        parent::__construct(self::ERROR_MESSAGE, ApiErrorCode::USER_NOT_FOUND);
    }
}
