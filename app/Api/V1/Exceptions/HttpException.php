<?php

namespace App\Api\V1\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException as SymphonyHttpException;

class HttpException extends SymphonyHttpException {

    public function __construct($message, $errorCode, $statusCode = 422) {
        parent::__construct($statusCode, $message, null, array(), $errorCode);
    }
}

class ApiErrorCode {
    // All Auth Related Error Follows here
    const AUTH_GENERIC            = 100;
    const AUTH_INVALID_CREDENTIAL = 101;

    // All User Related Error Follows here
    const USER_GENERIC   = 200;
    const USER_NOT_FOUND = 201;

    // All Rent Property Related Error Follows here
    const RENT_PROPERTY_NOT_FOUND = 300;

    // Service Related Exceptions
    const SERVICE_NOT_FOUND = 400;

    //Project Related Exception
    const PROJECT_NOT_FOUND = 500;

    // Buy Property Related Error
    const BUY_PROPERTY_NOT_FOUND = 600;

}
