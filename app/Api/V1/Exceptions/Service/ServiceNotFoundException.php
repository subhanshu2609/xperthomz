<?php


namespace App\Api\V1\Exceptions\Service;


use App\Api\V1\Exceptions\ApiErrorCode;
use App\Api\V1\Exceptions\ModelNotFoundException;

class ServiceNotFoundException extends ModelNotFoundException
{
    const ERROR_MESSAGE = "Requested Service does not exist.";

    public function __construct() {
        parent::__construct(self::ERROR_MESSAGE, ApiErrorCode::SERVICE_NOT_FOUND);
    }
}