<?php


namespace App\Api\V1\Exceptions\RentProperty;


use App\Api\V1\Exceptions\ApiErrorCode;
use App\Api\V1\Exceptions\ModelNotFoundException;

class RentPropertyNotFoundException extends ModelNotFoundException {
    const ERROR_MESSAGE = "Requested Rent Property Does not exist.";

    public function __construct() {
        parent::__construct(self::ERROR_MESSAGE, ApiErrorCode::RENT_PROPERTY_NOT_FOUND);
    }
}