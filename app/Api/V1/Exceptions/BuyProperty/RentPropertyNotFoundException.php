<?php


namespace App\Api\V1\Exceptions\RentProperty;


use App\Api\V1\Exceptions\ApiErrorCode;
use App\Api\V1\Exceptions\ModelNotFoundException;

class BuyPropertyNotFoundException extends ModelNotFoundException {
    const ERROR_MESSAGE = "Requested Buy Property Does not exist.";

    public function __construct() {
        parent::__construct(self::ERROR_MESSAGE, ApiErrorCode::BUY_PROPERTY_NOT_FOUND);
    }
}