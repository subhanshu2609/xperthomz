<?php


namespace App\Api\V1\Exceptions\Project;


use App\Api\V1\Exceptions\ApiErrorCode;
use App\Api\V1\Exceptions\ModelNotFoundException;

class ProjectNotFoundException extends ModelNotFoundException
{
    const ERROR_MESSAGE = "Requested Project Does not exist.";

    public function __construct() {
        parent::__construct(self::ERROR_MESSAGE, ApiErrorCode::PROJECT_NOT_FOUND);
    }
}