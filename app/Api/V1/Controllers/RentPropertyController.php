<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Exceptions\RentProperty\RentPropertyNotFoundException;
use App\Api\V1\Requests\RentPropertyCreateRequest;
use App\Api\V1\Requests\RentPropertyUpdateRequest;
use App\Api\V1\Transformers\RentPropertyTransformer;
use App\RentProperty;
use App\Services\Contracts\RentPropertyUpdateContract;
use App\Services\Entities\RentPropertyService;
use Illuminate\Support\Facades\Auth;

class RentPropertyController extends BaseController {

    public function show(RentPropertyService $rentPropertyService, $rentPropertyId) {
        $rentProperty = $rentPropertyService->show($rentPropertyId);

        if (!$rentProperty) {
            throw new RentPropertyNotFoundException();
        }
        return $this->response->item($rentProperty, new RentPropertyTransformer());
    }

    public function index(RentPropertyService $rentPropertyService) {
        $rentProperties = $rentPropertyService->index();
        return $this->response->collection($rentProperties, new RentPropertyTransformer());
    }
    public function create(RentPropertyService $rentPropertyService, RentPropertyCreateRequest $request) {
        $user = Auth::user();
        $rentProperty = $rentPropertyService->create($user->id, $request);
        return $this->response->item($rentProperty, new RentPropertyTransformer());
    }

    public function update(RentPropertyService $rentPropertyService, RentPropertyUpdateRequest $request, $rentPropertyId) {
        $rentProperty = $rentPropertyService->show($rentPropertyId);
        if (!$rentProperty) {
            throw new RentPropertyNotFoundException();
        }
        $updatedRentProperty = $rentPropertyService->update($request, $rentProperty);
        return $this->response->item($updatedRentProperty, new RentPropertyTransformer());
    }

    public function delete(RentPropertyService $rentPropertyService, $rentPropertyId) {
        $rentProperty = $rentPropertyService->show($rentPropertyId);

        if (!$rentProperty) {
            throw new RentPropertyNotFoundException();
        }
        $rentPropertyService->delete($rentProperty);
    }
}