<?php
/**
 * Created by PhpStorm.
 * User: piyushkantm
 * Date: 29/11/18
 * Time: 11:34 AM
 */

namespace App\Api\V1\Controllers;


use App\Api\V1\Exceptions\User\UserNotFoundException;
use App\Api\V1\Requests\UserCreateRequest;
use App\Api\V1\Requests\UserUpdateRequest;
use App\Api\V1\Transformers\UserTransformer;
use App\Services\Entities\UserService;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController {

    public function showMe() {
        $user = Auth::user();

        return $this->response->item($user, new UserTransformer());
    }

    public function updateMe(UserService $userService, UserUpdateRequest $request) {
        $userId = Auth::id();
        $user   = $userService->showUserById($userId);

        $updatedUser = $userService->update($user, $request);

        return $this->response->item($updatedUser, new UserTransformer());
    }

    public function show(UserService $userService, $userId) {
        $user = $userService->showUserById($userId);

        if (!$user) {
            throw new UserNotFoundException();
        }

        return $this->response->item($user, new UserTransformer());
    }

    public function index(UserService $userService) {
        $users = $userService->index();

        return $this->response->collection($users, new UserTransformer());
    }
    public function create(UserService $userService, UserCreateRequest $request) {
        $user = $userService->create($request);

        return $this->response->item($user, new UserTransformer());
    }

    public function update(UserService $userService, UserUpdateRequest $request, $userId) {
        $user   = $userService->showUserById($userId);

        $updatedUser = $userService->update($user, $request);

        return $this->response->item($updatedUser, new UserTransformer());
    }

    public function delete(UserService $userService, $userId) {
        $user = $userService->showUserById($userId);

        $userService->delete($user);
    }
}
