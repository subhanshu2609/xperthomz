<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Requests\ProjectCreateRequest;
use App\Api\V1\Requests\ProjectUpdateRequest;
use App\Api\V1\Transformers\ProjectTransformer;
use App\Services\Entities\ProjectService;

class ProjectController extends BaseController
{
    public function create(ProjectService $projectService, ProjectCreateRequest $request)
    {
        $project = $projectService->create($request);

        return $this->response->item($project, new ProjectTransformer());
    }

    public function  update(ProjectService $projectService, ProjectUpdateRequest $request,$projectId)
    {
        $project = $projectService->show($projectId);

        $project = $projectService->update($project, $request);

        return $this->response->item($project, new ProjectTransformer());
    }

    public function index(ProjectService $projectService)
    {
        $project = $projectService->index();

        return $this->response->collection($project, new ProjectTransformer());
    }

    public function delete(ProjectService $projectService, $projectId)
    {
        $project = $projectService->show($projectId);

        $projectService->delete($project);
    }
}
