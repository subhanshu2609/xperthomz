<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Requests\KnowledgeCenterCreateRequest;
use App\Api\V1\Requests\KnowledgeCenterUpdateRequest;
use App\Api\V1\Transformers\KnowledgeCenterTransformer;
use App\Services\Entities\KnowledgeCenterService;

class KnowledgeCenterController extends BaseController {

    public function create(KnowledgeCenterService $knowledgeCenterService, KnowledgeCenterCreateRequest $request) {
        $knowledgeCenter = $knowledgeCenterService->create($request);

        return $this->response->item($knowledgeCenter, new KnowledgeCenterTransformer());
    }

    public function  update(KnowledgeCenterService $knowledgeCenterService, KnowledgeCenterUpdateRequest $request,$knowledgeCenterId) {
        $knowledgeCenter = $knowledgeCenterService->show($knowledgeCenterId);

        $knowledgeCenter = $knowledgeCenterService->update($knowledgeCenter, $request);

        return $this->response->item($knowledgeCenter, new KnowledgeCenterTransformer());
    }

    public function index(KnowledgeCenterService $knowledgeCenterService) {
        $knowledgeCenter = $knowledgeCenterService->index();

        return $this->response->collection($knowledgeCenter, new KnowledgeCenterTransformer());
    }

    public function delete(KnowledgeCenterService $knowledgeCenterService, $knowledgeCenterId) {
        $knowledgeCenter = $knowledgeCenterService->show($knowledgeCenterId);

        $knowledgeCenterService->delete($knowledgeCenter);
    }
}
