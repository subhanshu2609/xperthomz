<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Exceptions\RentProperty\BuyPropertyNotFoundException;
use App\Api\V1\Requests\BuyPropertyCreateRequest;
use App\Api\V1\Requests\BuyPropertyUpdateRequest;
use App\Api\V1\Transformers\BuyPropertyTransformer;
use App\Services\Entities\BuyPropertyService;
use Illuminate\Support\Facades\Auth;

class BuyPropertyController extends BaseController {

    public function show(BuyPropertyService $buyPropertyService, $buyPropertyId) {
        $buyProperty = $buyPropertyService->show($buyPropertyId);

        if (!$buyProperty) {
            throw new BuyPropertyNotFoundException();
        }
        return $this->response->item($buyProperty, new BuyPropertyTransformer());
    }

    public function index(BuyPropertyService $buyPropertyService) {
        $buyProperties = $buyPropertyService->index();
        return $this->response->collection($buyProperties, new BuyPropertyTransformer());
    }

    public function create(BuyPropertyService $buyPropertyService, BuyPropertyCreateRequest $request) {
        $user = Auth::user();
        $buyProperty = $buyPropertyService->create($user->id, $request);
        return $this->response->item($buyProperty, new BuyPropertyTransformer());
    }

    public function update(BuyPropertyService $buyPropertyService, BuyPropertyUpdateRequest $request, $buyPropertyId) {
        $buyProperty = $buyPropertyService->show($buyPropertyId);
        if (!$buyProperty) {
            throw new BuyPropertyNotFoundException();
        }
        $updatedBuyProperty = $buyPropertyService->update($request, $buyProperty);
        return $this->response->item($updatedBuyProperty, new BuyPropertyTransformer());
    }

    public function delete(BuyPropertyService $buyPropertyService, $buyPropertyId) {
        $buyProperty = $buyPropertyService->show($buyPropertyId);

        if (!$buyProperty) {
            throw new BuyPropertyNotFoundException();
        }
        $buyPropertyService->delete($buyProperty);
    }
}