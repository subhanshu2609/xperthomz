<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Exceptions\Auth\InvalidCredentialException;
use App\Api\V1\Exceptions\User\UserNotFoundException;
use App\Api\V1\Requests\BaseRequest;
use App\Api\V1\Transformers\UserTransformer;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends BaseController {
    /**
     * Login User
     *
     * Returns the full User Object with the login token used to access user account.
     *
     * @param BaseRequest $request
     * @throws InvalidCredentialException
     * @throws \Illuminate\Validation\ValidationException
     * @return array
     */
    public function authenticate(BaseRequest $request) {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                throw new InvalidCredentialException();
            }
        } catch (JWTException $e) {
            throw new InvalidCredentialException();
        }

        $user = User::where('email', $request->get('email'))
            ->first();

        $userTransformer = new UserTransformer();
        $transformedUser = $userTransformer->transform($user);

        return [
            'token' => $token,
            'user'  => $transformedUser
        ];
    }

    /**
     * Generate User Token
     *
     * Generates the token for the given user Id.
     *
     * @param $userId
     * @throws UserNotFoundException
     * @return string
     */
    public function generateToken($userId) {
        $user = User::whereId($userId)->first();

        if (!$user) {
            throw new UserNotFoundException();
        }

        return JWTAuth::fromUser($user);
    }
}
