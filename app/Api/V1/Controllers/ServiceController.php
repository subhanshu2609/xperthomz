<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Requests\ServiceCreateRequest;
use App\Api\V1\Requests\ServiceUpdateRequest;
use App\Api\V1\Transformers\ServiceTransformer;
use App\Services\Entities\ServiceService;

class ServiceController extends BaseController {
    public function create(ServiceService $serviceService, ServiceCreateRequest $request) {
        $service = $serviceService->create($request);

        return $this->response->item($service, new ServiceTransformer());
    }

    public function update(ServiceService $serviceService, ServiceUpdateRequest $request, $serviceId)
    {
        $service = $serviceService->show($serviceId);

        $updatedService = $serviceService->update($service, $request);

        return $this->response->item($updatedService, new ServiceTransformer());

    }

    public function index(ServiceService $serviceService)
    {
     $services = $serviceService->index();

     return $this->response->collection($services, new ServiceTransformer());
    }

    public function delete(ServiceService $serviceService, $serviceId)
    {
        $service = $serviceService->show($serviceId);
        $serviceService->delete($service);

    }
}
