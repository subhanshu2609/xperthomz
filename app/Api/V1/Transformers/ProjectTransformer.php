<?php


namespace App\Api\V1\Transformers;


use App\Project;
use League\Fractal\TransformerAbstract;

class ProjectTransformer extends TransformerAbstract
{
    public function transform(Project $project)
    {
        return [
            'id' => $project->id,
            'name'  => $project->name,
            'category' => $project->category,
            'area' => $project->area,
            'address' => $project->address,
            'description' => $project->description,
            'status' => $project->status,
            'created_at' => $project->created_at_dt
        ];
    }
}
