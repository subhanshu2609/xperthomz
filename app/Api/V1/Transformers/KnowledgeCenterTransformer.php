<?php


namespace App\Api\V1\Transformers;


use App\KnowledgeCenter;
use League\Fractal\TransformerAbstract;

class KnowledgeCenterTransformer extends TransformerAbstract
{
    public function transform(KnowledgeCenter $knowledgeCenter)
    {
        return[
            'id' => $knowledgeCenter->id,
            'category'=> $knowledgeCenter-> category,
            'headline'=> $knowledgeCenter-> headline,
            'description'=> $knowledgeCenter-> description,
            'created_at' => $knowledgeCenter->created_at_dt
        ];
    }
}
