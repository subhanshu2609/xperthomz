<?php

namespace App\Api\V1\Transformers;


use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract {
    public function transform(User $user) {
        return [
            'id'            => $user->id,
            'first_name'    => $user->first_name,
            'last_name'     => $user->last_name,
            'email'         => $user->email,
            'phone_number'  => $user->phone_number,
            'type'          => $user->type,
            'created_at'    => $user->created_at_dt
        ];
    }
}
