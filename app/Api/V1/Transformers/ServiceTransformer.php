<?php


namespace App\Api\V1\Transformers;


use App\Service;
use League\Fractal\TransformerAbstract;

class ServiceTransformer extends TransformerAbstract
{
    public function transform(Service $service)
    {
        return [
            'id' => $service->id,
            'heading' => $service->heading,
            'description' => $service->description,
            'created_at' => $service->created_at_dt
        ];
    }
}
