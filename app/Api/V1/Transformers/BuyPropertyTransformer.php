<?php


namespace App\Api\V1\Transformers;


use App\BuyProperty;
use League\Fractal\TransformerAbstract;

class BuyPropertyTransformer extends TransformerAbstract {

    public function transform(BuyProperty $buyProperty) {
        return [
            'id' => $buyProperty->id,
            'title' => $buyProperty->title,
            'owner_id' => $buyProperty->owner_id,
            'property_type' => $buyProperty->property_type,
            'featured_property' => $buyProperty->featured_property,
            'property_category' => $buyProperty->property_category,
            'property_status' => $buyProperty->property_status,
            'address_1' => $buyProperty->address_1,
            'address_2' => $buyProperty->address_2,
            'accommodation' => $buyProperty->accommodation,
            'facing' => $buyProperty->facing,
            'total_sqft' => $buyProperty->total_sqft,
            'furnished_status' => $buyProperty->furnished_status,
            'balcony' => $buyProperty->balcony,
            'overlooking' => $buyProperty->overlooking,
            'water_availability' => $buyProperty->water_availability,
            'power_backup' => $buyProperty->power_backup,
            'landmark' => $buyProperty->landmark,
            'parking' => $buyProperty->parking,
            'maintenance_pmo' => $buyProperty->maintenance_pmo,
            'construction_age' => $buyProperty->construction_age,
            'rera_id' => $buyProperty->rera_id,
            'expected_price' => $buyProperty->expected_price,
            'registry_status' => $buyProperty->registry_status,
            'rental_value' => $buyProperty->rental_value,
            'description' => $buyProperty->description,
            'exclusive' => $buyProperty->exclusive,
            'created_at' => $buyProperty->created_at_dt,
            'updated_at' => $buyProperty->updated_at_dt
        ];
    }
}