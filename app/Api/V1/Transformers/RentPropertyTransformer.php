<?php


namespace App\Api\V1\Transformers;


use App\RentProperty;
use League\Fractal\TransformerAbstract;

class RentPropertyTransformer extends TransformerAbstract {

    public function transform(RentProperty $rentProperty) {
        return [
            'id' => $rentProperty->id,
            'title' => $rentProperty->title,
            'owner_id' => $rentProperty->owner_id,
            'property_type' => $rentProperty->property_type,
            'rent_type' => $rentProperty->rent_type,
            'featured_property' => $rentProperty->featured_property,
            'property_category' => $rentProperty->property_category,
            'address_1' => $rentProperty->address_1,
            'address_2' => $rentProperty->address_2,
            'accommodation' => $rentProperty->accommodation,
            'facing' => $rentProperty->facing,
            'total_sqft' => $rentProperty->total_sqft,
            'furnished_status' => $rentProperty->furnished_status,
            'balcony' => $rentProperty->balcony,
            'overlooking' => $rentProperty->overlooking,
            'water_availability' => $rentProperty->water_availability,
            'power_backup' => $rentProperty->power_backup,
            'landmark' => $rentProperty->landmark,
            'parking' => $rentProperty->parking,
            'maintenance_pmo' => $rentProperty->maintenance_pmo,
            'construction_age' => $rentProperty->construction_age,
            'expected_rent' => $rentProperty->expected_rent,
            'availability' => $rentProperty->availability,
            'tenant_type' => $rentProperty->tenant_type,
            'description' => $rentProperty->description,
            'exclusive' => $rentProperty->exclusive,
            'created_at' => $rentProperty->created_at_dt
        ];
    }
}