<?php


namespace App\Api\V1\Requests;


use App\Services\Contracts\UserCreateContract;

class UserCreateRequest extends BaseRequest implements UserCreateContract {
    const FIRST_NAME    = 'first_name';
    const LAST_NAME     = 'last_name';
    const EMAIL         = 'email';
    const PHONE_NUMBER  = 'phone_number';
    const TYPE          = 'type';
    const PASSWORD      = 'password';

    public function rules() {
        return [
            self::FIRST_NAME    => 'required|string',
            self::LAST_NAME     => 'nullable|string',
            self::EMAIL         => 'required|email',
            self::PHONE_NUMBER  => 'required|string',
            self::TYPE          => 'required|valid_user_types',
            self::PASSWORD      => 'required|string',
        ];
    }

    public function hasLastName() {
        return $this->has(self::LAST_NAME);
    }

    public function getFirstName() {
        return $this->get(self::FIRST_NAME);
    }

    public function getLastName() {
        return $this->get(self::LAST_NAME);
    }

    public function getEmail() {
        return $this->get(self::EMAIL);
    }

    public function getPhoneNumber() {
        return $this->get(self::PHONE_NUMBER);
    }

    public function getType() {
        return $this->get(self::TYPE);
    }

    public function getPassword() {
        return $this->get(self::PASSWORD);
    }
}