<?php


namespace App\Api\V1\Requests;


use App\Services\Contracts\ServiceCreateContract;

class ServiceCreateRequest extends BaseRequest implements ServiceCreateContract
{
    const HEADING = 'heading';
    const DESCRIPTION = 'description';

    public function rules() {
        return[
            self::HEADING => 'required|string',
            self::DESCRIPTION => 'required|string'
        ];
    }

    public function getHeading() {
        return $this->get(self::HEADING);
    }

    public function getDescription() {
        return $this->get(self::DESCRIPTION);
    }

}
