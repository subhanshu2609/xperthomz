<?php


namespace App\Api\V1\Requests;


use App\Services\Contracts\ProjectUpdateContract;

class ProjectUpdateRequest extends BaseRequest implements ProjectUpdateContract {
    const NAME = 'name';
    const CATEGORY = 'category';
    const AREA = 'area';
    const ADDRESS = 'address';
    const DESCRIPTION = 'description';
    const STATUS = 'status';
    const TYPE = 'type';
    const PRICE_PER_SQFT = 'price_per_sqft';
    const TOTAL_SQFT = 'total_sqft';
    const FURNISHED_STATUS = 'furnished_status';
    const FEATURE_NAME = 'feature_name';
    const TOTAL_FLATS = 'total_flats';
    const TOWER_COUNT = 'tower_count';
    const PRICE_RANGE = 'price_range';
    const SIZE_RANGE = 'size_range';

    //this function applies validation for the rest of the functions
    public function rules()
    {
        return [
            self::NAME => 'required|string',
            self::CATEGORY=> 'required|valid_project_categories',
            self::AREA=> 'required|integer',
            self::ADDRESS=>'required|string',
            self::DESCRIPTION=>'nullable|string',
            self::STATUS=>'required|valid_project_status',
            self::TYPE => 'required|valid_accommodation_types',
            self::PRICE_PER_SQFT => 'required|integer',
            self::TOTAL_SQFT => 'required|integer',
            self::FURNISHED_STATUS => 'required|valid_furnished_types',
            self::FEATURE_NAME => 'required|string',
            self::TOTAL_FLATS => 'required|integer',
            self::TOWER_COUNT => 'required|integer',
            self::PRICE_RANGE => 'required|string',
            self::SIZE_RANGE => 'required|string',
        ];
    }

    public function hasName()
    {
        return $this->has(self::NAME);
    }

    public function hasCategory()
    {
        return $this->has(self::CATEGORY);
    }

    public function hasArea()
    {
        return $this->has(self::AREA);
    }

    public function hasAddress()
    {
        return $this->has(self::ADDRESS);
    }

    public function hasDescription()
    {
        return $this->has(self::DESCRIPTION);
    }

    public function hasStatus()
    {
        return $this->has(self::STATUS);
    }

    public function hasType()
    {
        return $this->has(self::TYPE);
    }

    public function hasPricePerSqft()
    {
        return $this->has(self::PRICE_PER_SQFT);
    }

    public function hasTotalSqft()
    {
        return $this->has(self::TOTAL_SQFT);
    }

    public function hasFurnishedStatus()
    {
        return $this->has(self::FURNISHED_STATUS);
    }

    public function hasFeatureName()
    {
        return $this->has(self::FEATURE_NAME);
    }

    public function hasTotalFlats()
    {
        return $this->has(self::TOTAL_FLATS);
    }

    public function hasTowerCount()
    {
        return $this->has(self::TOWER_COUNT);
    }

    public function hasPriceRange()
    {
        return $this->has(self::PRICE_RANGE);
    }

    public function hasSizeRange()
    {
        return $this->has(self::SIZE_RANGE);
    }

    public function getName()
    {
        return $this->get(self::NAME);
    }

    public function getCategory()
    {
        return $this->get(self::CATEGORY);
    }

    public function getArea()
    {
        return $this->get(self::AREA);
    }

    public function getAddress()
    {
        return $this->get(self::ADDRESS);
    }

    public function getDescription()
    {
        return $this->get(self::DESCRIPTION);
    }

    public function getStatus()
    {
        return $this->get(self::STATUS);
    }

    public function getType()
    {
        return $this->get(self::TYPE);
    }

    public function getPricePerSqft()
    {
        return $this->get(self::PRICE_PER_SQFT);
    }

    public function getTotalSqft()
    {
        return $this->get(self::TOTAL_SQFT);
    }

    public function getFurnishedStatus()
    {
        return $this->get(self::FURNISHED_STATUS);
    }

    public function getFeatureName()
    {
        return $this->get(self::FEATURE_NAME);
    }

    public function getTotalFlats()
    {
        return $this->get(self::TOTAL_FLATS);
    }

    public function getTowerCount()
    {
        return $this->get(self::TOWER_COUNT);
    }

    public function getPriceRange()
    {
        return $this->get(self::PRICE_RANGE);
    }

    public function getSizeRange()
    {
        return $this->get(self::SIZE_RANGE);
    }
}
