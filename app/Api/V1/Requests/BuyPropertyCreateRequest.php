<?php


namespace App\Api\V1\Requests;


use App\Services\Contracts\BuyPropertyCreateContract;

class BuyPropertyCreateRequest extends BaseRequest implements BuyPropertyCreateContract {

    const TITLE = 'title';
    const PROPERTY_TYPE = 'property_type';
    const FEATURED_PROPERTY = 'featured_property';
    const PROPERTY_CATEGORY = 'property_category';
    const PROPERTY_STATUS = 'property_status';
    const ADDRESS_1 = 'address_1';
    const ADDRESS_2 = 'address_2';
    const PROJECT_ID = 'project_id';
    const ACCOMMODATION = 'accommodation';
    const FACING = 'facing';
    const TOTAL_SQFT = 'total_sqft';
    const FURNISHED_STATUS = 'furnished_status';
    const BALCONY = 'balcony';
    const OVERLOOKING = 'overlooking';
    const WATER_AVAILABILITY = 'water_availability';
    const POWER_BACKUP = 'power_backup';
    const LANDMARK = 'landmark';
    const PARKING = 'parking';
    const MAINTENANCE_PMO = 'maintenance_pmo';
    const CONSTRUCTION_AGE = 'construction_age';
    const RERA_ID = 'rera_id';
    const EXPECTED_PRICE = 'expected_price';
    const REGISTRY_STATUS = 'registry_status';
    const RENTAL_VALUE = 'rental_value';
    const DESCRIPTION = 'description';
    const EXCLUSIVE = 'exclusive';

    public function rules() {
        return [
            self::TITLE => 'required|string',
            self::PROPERTY_TYPE => 'required|valid_property_types',
            self::FEATURED_PROPERTY => 'required|boolean',
            self::PROPERTY_CATEGORY => 'required|valid_property_categories',
            self::PROPERTY_STATUS => 'required|valid_project_status',
            self::ADDRESS_1 => 'required|string',
            self::ADDRESS_2 => 'required|string',
            self::PROJECT_ID => 'required|exists:projects,id',
            self::ACCOMMODATION => 'required|valid_accommodation_types',
            self::FACING => 'required|valid_facing_types',
            self::TOTAL_SQFT => 'required|numeric',
            self::FURNISHED_STATUS => 'required|valid_furnished_types',
            self::BALCONY => 'required|numeric',
            self::OVERLOOKING => 'required|valid_overlooking_types',
            self::WATER_AVAILABILITY => 'required|boolean',
            self::POWER_BACKUP => 'required|boolean',
            self::LANDMARK => 'nullable|string',
            self::PARKING => 'required|boolean',
            self::MAINTENANCE_PMO => 'required|numeric',
            self::CONSTRUCTION_AGE => 'nullable|numeric',
            self::RERA_ID => 'required|numeric',
            self::EXPECTED_PRICE => 'required|numeric',
            self::REGISTRY_STATUS => 'required|boolean',
            self::RENTAL_VALUE => 'required|numeric',
            self::DESCRIPTION => 'nullable|string',
            self::EXCLUSIVE => 'required|boolean',
        ];
    }

    public function hasLandmark() {
        return $this->has(self::LANDMARK);
    }

    public function hasConstructionAge() {
        return $this->has(self::CONSTRUCTION_AGE);
    }

    public function hasDescription() {
        return $this->has(self::DESCRIPTION);
    }

    public function getTitle() {
        return $this->get(self::TITLE);
    }

    public function getPropertyType() {
        return $this->get(self::PROPERTY_TYPE);
    }

    public function getFeaturedProperty() {
        return $this->get(self::FEATURED_PROPERTY);
    }

    public function getPropertyCategory() {
        return $this->get(self::PROPERTY_CATEGORY);
    }

    public function getPropertyStatus() {
        return $this->get(self::PROPERTY_STATUS);
    }

    public function getAddress1() {
        return $this->get(self::ADDRESS_1);
    }

    public function getAddress2() {
        return $this->get(self::ADDRESS_2);
    }

    public function getProjectId () {
        return $this->get(self::PROJECT_ID);
    }

    public function getAccommodation() {
        return $this->get(self::ACCOMMODATION);
    }

    public function getFacing() {
        return $this->get(self::FACING);
    }

    public function getTotalSqft() {
        return $this->get(self::TOTAL_SQFT);
    }

    public function getFurnishedStatus() {
        return $this->get(self::FURNISHED_STATUS);
    }

    public function getBalcony() {
        return $this->get(self::BALCONY);
    }

    public function getOverlooking() {
        return $this->get(self::OVERLOOKING);
    }

    public function getWaterAvailability() {
        return $this->get(self::WATER_AVAILABILITY);
    }

    public function getPowerBackup() {
        return $this->get(self::POWER_BACKUP);
    }

    public function getLandmark() {
        return $this->get(self::LANDMARK);
    }

    public function getParking() {
        return $this->get(self::PARKING);
    }

    public function getMaintenancePmo() {
        return $this->get(self::MAINTENANCE_PMO);
    }

    public function getConstructionAge() {
        return $this->get(self::CONSTRUCTION_AGE);
    }

    public function getReraId() {
        return $this->get(self::RERA_ID);
    }

    public function getExpectedPrice() {
        return $this->get(self::EXPECTED_PRICE);
    }

    public function getRegistryStatus() {
        return $this->get(self::REGISTRY_STATUS);
    }

    public function getRentalValue() {
        return $this->get(self::RENTAL_VALUE);
    }

    public function getDescription() {
        return $this->get(self::DESCRIPTION);
    }

    public function getExclusive() {
        return $this->get(self::EXCLUSIVE);
    }
}