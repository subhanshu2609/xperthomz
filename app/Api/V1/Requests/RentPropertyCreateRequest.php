<?php


namespace App\Api\V1\Requests;


use App\Services\Contracts\RentPropertyCreateContract;

class RentPropertyCreateRequest extends BaseRequest implements RentPropertyCreateContract {

    const TITLE = 'title';
    const PROPERTY_TYPE = 'property_type';
    const RENT_TYPE = 'rent_type';
    const FEATURED_PROPERTY = 'featured_property';
    const PROPERTY_CATEGORY = 'property_category';
    const ADDRESS_1 = 'address_1';
    const ADDRESS_2 = 'address_2';
    const ACCOMMODATION = 'accommodation';
    const FACING = 'facing';
    const TOTAL_SQFT = 'total_sqft';
    const FURNISHED_STATUS = 'furnished_status';
    const BALCONY = 'balcony';
    const OVERLOOKING = 'overlooking';
    const WATER_AVAILABILITY = 'water_availability';
    const POWER_BACKUP = 'power_backup';
    const LANDMARK = 'landmark';
    const PARKING = 'parking';
    const MAINTENANCE_PMO = 'maintenance_pmo';
    const CONSTRUCTION_AGE = 'construction_age';
    const EXPECTED_RENT = 'expected_rent';
    const AVAILABILITY = 'availability';
    const TENANT_TYPE = 'tenant_type';
    const DESCRIPTION = 'description';
    const EXCLUSIVE = 'exclusive';

    public function rules() {
        return [
            self::TITLE => 'required|string',
            self::PROPERTY_TYPE => 'required|valid_property_types',
            self::RENT_TYPE => 'required|valid_rent_types',
            self::FEATURED_PROPERTY => 'required|boolean',
            self::PROPERTY_CATEGORY => 'required|valid_property_categories',
            self::ADDRESS_1 => 'required|string',
            self::ADDRESS_2 => 'required|string',
            self::ACCOMMODATION => 'required|valid_accommodation_types',
            self::FACING => 'required|valid_facing_types',
            self::TOTAL_SQFT => 'required|numeric',
            self::FURNISHED_STATUS => 'required|valid_furnished_types',
            self::BALCONY => 'required|numeric',
            self::OVERLOOKING => 'required|valid_overlooking_types',
            self::WATER_AVAILABILITY => 'required|boolean',
            self::POWER_BACKUP => 'required|boolean',
            self::LANDMARK => 'nullable|string',
            self::PARKING => 'required|boolean',
            self::MAINTENANCE_PMO => 'required|numeric',
            self::CONSTRUCTION_AGE => 'nullable|numeric',
            self::EXPECTED_RENT => 'required|numeric',
            self::AVAILABILITY => 'required|valid_availability_types',
            self::TENANT_TYPE => 'required|valid_tenant_types',
            self::DESCRIPTION => 'nullable|string',
            self::EXCLUSIVE => 'required|boolean',
        ];
    }

    public function hasLandmark() {
        return $this->has(self::LANDMARK);
    }

    public function hasConstructionAge() {
        return $this->has(self::CONSTRUCTION_AGE);
    }

    public function hasDescription() {
        return $this->has(self::DESCRIPTION);
    }

    public function getTitle() {
        return $this->get(self::TITLE);
    }

    public function getPropertyType() {
        return $this->get(self::PROPERTY_TYPE);
    }

    public function getRentType() {
        return $this->get(self::RENT_TYPE);
    }

    public function getFeaturedProperty() {
        return $this->get(self::FEATURED_PROPERTY);
    }
    public function getPropertyCategory() {
        return $this->get(self::PROPERTY_CATEGORY);
    }

    public function getAddress1() {
        return $this->get(self::ADDRESS_1);
    }

    public function getAddress2() {
        return $this->get(self::ADDRESS_2);
    }

    public function getAccommodation() {
        return $this->get(self::ACCOMMODATION);
    }

    public function getFacing() {
        return $this->get(self::FACING);
    }

    public function getTotalSqft() {
        return $this->get(self::TOTAL_SQFT);
    }

    public function getFurnishedStatus() {
        return $this->get(self::FURNISHED_STATUS);
    }

    public function getBalcony() {
        return $this->get(self::BALCONY);
    }

    public function getOverlooking() {
        return $this->get(self::OVERLOOKING);
    }

    public function getWaterAvailability() {
        return $this->get(self::WATER_AVAILABILITY);
    }

    public function getPowerBackup() {
        return $this->get(self::POWER_BACKUP);
    }

    public function getLandmark() {
        return $this->get(self::LANDMARK);
    }

    public function getParking() {
        return $this->get(self::PARKING);
    }

    public function getMaintenancePmo() {
        return $this->get(self::MAINTENANCE_PMO);
    }

    public function getConstructionAge() {
        return $this->get(self::CONSTRUCTION_AGE);
    }

    public function getExpectedRent() {
        return $this->get(self::EXPECTED_RENT);
    }

    public function getAvailability() {
        return $this->get(self::AVAILABILITY);
    }

    public function getTenantType() {
        return $this->get(self::TENANT_TYPE);
    }

    public function getDescription() {
        return $this->get(self::DESCRIPTION);
    }

    public function getExclusive() {
        return $this->get(self::EXCLUSIVE);
    }
}