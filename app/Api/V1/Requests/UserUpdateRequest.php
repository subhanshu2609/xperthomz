<?php


namespace App\Api\V1\Requests;


use App\Services\Contracts\UserUpdateContract;

class UserUpdateRequest extends BaseRequest implements UserUpdateContract {
    const FIRST_NAME    = 'first_name';
    const LAST_NAME     = 'last_name';
    const EMAIL         = 'email';
    const PHONE_NUMBER  = 'phone_number';
    const TYPE          = 'type';
    const PASSWORD      = 'password';

    public function rules() {
        return [
            self::FIRST_NAME    => 'nullable|string',
            self::LAST_NAME     => 'nullable|string',
            self::EMAIL         => 'nullable|email',
            self::PHONE_NUMBER  => 'nullable|string',
            self::TYPE          => 'nullable|valid_user_types',
            self::PASSWORD      => 'nullable|string',
        ];
    }

    public function hasFirstName() {
        return $this->has(self::FIRST_NAME);
    }

    public function hasLastName() {
        return $this->has(self::LAST_NAME);
    }

    public function hasEmail() {
        return $this->has(self::EMAIL);
    }

    public function hasPhoneNumber() {
        return $this->has(self::PHONE_NUMBER);
    }

    public function hasType() {
        return $this->has(self::TYPE);
    }

    public function hasPassword() {
        return $this->has(self::PASSWORD);
    }

    public function getFirstName() {
        return $this->get(self::FIRST_NAME);
    }

    public function getLastName() {
        return $this->get(self::LAST_NAME);
    }

    public function getEmail() {
        return $this->get(self::EMAIL);
    }

    public function getPhoneNumber() {
        return $this->get(self::PHONE_NUMBER);
    }

    public function getType() {
        return $this->get(self::TYPE);
    }

    public function getPassword() {
        return $this->get(self::PASSWORD);
    }
}