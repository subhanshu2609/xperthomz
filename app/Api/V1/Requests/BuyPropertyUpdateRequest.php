<?php


namespace App\Api\V1\Requests;


use App\Services\Contracts\BuyPropertyUpdateContract;

class BuyPropertyUpdateRequest extends BaseRequest implements BuyPropertyUpdateContract {

    const TITLE = 'title';
    const PROPERTY_TYPE = 'property_type';
    const FEATURED_PROPERTY = 'featured_property';
    const PROPERTY_CATEGORY = 'property_category';
    const PROPERTY_STATUS = 'property_status';
    const ADDRESS_1 = 'address_1';
    const ADDRESS_2 = 'address_2';
    const ACCOMMODATION = 'accommodation';
    const FACING = 'facing';
    const TOTAL_SQFT = 'total_sqft';
    const FURNISHED_STATUS = 'furnished_status';
    const BALCONY = 'balcony';
    const OVERLOOKING = 'overlooking';
    const WATER_AVAILABILITY = 'water_availability';
    const POWER_BACKUP = 'power_backup';
    const LANDMARK = 'landmark';
    const PARKING = 'parking';
    const MAINTENANCE_PMO = 'maintenance_pmo';
    const CONSTRUCTION_AGE = 'construction_age';
    const RERA_ID = 'rera_id';
    const EXPECTED_PRICE = 'expected_price';
    const REGISTRY_STATUS = 'registry_status';
    const RENTAL_VALUE = 'rental_value';
    const DESCRIPTION = 'description';
    const EXCLUSIVE = 'exclusive';

    public function rules() {
        return [
            self::TITLE => 'nullable|string',
            self::PROPERTY_TYPE => 'nullable|valid_property_types',
            self::FEATURED_PROPERTY => 'nullable|boolean',
            self::PROPERTY_CATEGORY => 'nullable|valid_property_categories',
            self::PROPERTY_STATUS => 'nullable|valid_project_status',
            self::ADDRESS_1 => 'nullable|string',
            self::ADDRESS_2 => 'nullable|string',
            self::ACCOMMODATION => 'nullable|valid_accommodation_types',
            self::FACING => 'nullable|valid_facing_types',
            self::TOTAL_SQFT => 'nullable|numeric',
            self::FURNISHED_STATUS => 'nullable|valid_furnished_types',
            self::BALCONY => 'nullable|numeric',
            self::OVERLOOKING => 'nullable|valid_overlooking_types',
            self::WATER_AVAILABILITY => 'nullable|boolean',
            self::POWER_BACKUP => 'nullable|boolean',
            self::LANDMARK => 'nullable|string',
            self::PARKING => 'nullable|boolean',
            self::MAINTENANCE_PMO => 'nullable|numeric',
            self::CONSTRUCTION_AGE => 'nullable|numeric',
            self::RERA_ID => 'nullable|numeric',
            self::EXPECTED_PRICE => 'nullable|numeric',
            self::REGISTRY_STATUS => 'nullable|boolean',
            self::RENTAL_VALUE => 'nullable|numeric',
            self::DESCRIPTION => 'nullable|string',
            self::EXCLUSIVE => 'nullable|boolean',
        ];
    }

    public function hasTitle() {
        return $this->has(self::TITLE);
    }

    public function hasPropertyType() {
        return $this->has(self::PROPERTY_TYPE);
    }

    public function hasFeaturedProperty() {
        return $this->has(self::FEATURED_PROPERTY);
    }

    public function hasPropertyCategory() {
        return $this->has(self::PROPERTY_CATEGORY);
    }

    public function hasPropertyStatus() {
        return $this->has(self::PROPERTY_STATUS);
    }

    public function hasAddress1() {
        return $this->has(self::ADDRESS_1);
    }

    public function hasAddress2() {
        return $this->has(self::ADDRESS_2);
    }

    public function hasAccommodation() {
        return $this->has(self::ACCOMMODATION);
    }

    public function hasFacing() {
        return $this->has(self::FACING);
    }

    public function hasTotalSqft() {
        return $this->has(self::TOTAL_SQFT);
    }

    public function hasFurnishedStatus() {
        return $this->has(self::FURNISHED_STATUS);
    }

    public function hasBalcony() {
        return $this->has(self::BALCONY);
    }

    public function hasOverlooking() {
        return $this->has(self::OVERLOOKING);
    }

    public function hasWaterAvailability() {
        return $this->has(self::WATER_AVAILABILITY);
    }

    public function hasPowerBackup() {
        return $this->has(self::POWER_BACKUP);
    }

    public function hasLandmark() {
        return $this->has(self::LANDMARK);
    }

    public function hasParking() {
        return $this->has(self::PARKING);
    }

    public function hasMaintenancePmo() {
        return $this->has(self::MAINTENANCE_PMO);
    }

    public function hasConstructionAge() {
        return $this->has(self::CONSTRUCTION_AGE);
    }

    public function hasReraId() {
        return $this->has(self::RERA_ID);
    }

    public function hasExpectedPrice() {
        return $this->has(self::EXPECTED_PRICE);
    }

    public function hasRegistryStatus() {
        return $this->has(self::REGISTRY_STATUS);
    }

    public function hasRentalValue() {
        return $this->has(self::RENTAL_VALUE);
    }

    public function hasDescription() {
        return $this->has(self::DESCRIPTION);
    }

    public function hasExclusive() {
        return $this->has(self::EXCLUSIVE);
    }

    public function getTitle() {
        return $this->get(self::TITLE);
    }

    public function getPropertyType() {
        return $this->get(self::PROPERTY_TYPE);
    }

    public function getFeaturedProperty() {
        return $this->get(self::FEATURED_PROPERTY);
    }

    public function getPropertyCategory() {
        return $this->get(self::PROPERTY_CATEGORY);
    }

    public function getPropertyStatus() {
        return $this->get(self::PROPERTY_STATUS);
    }

    public function getAddress1() {
        return $this->get(self::ADDRESS_1);
    }

    public function getAddress2() {
        return $this->get(self::ADDRESS_2);
    }

    public function getAccommodation() {
        return $this->get(self::ACCOMMODATION);
    }

    public function getFacing() {
        return $this->get(self::FACING);
    }

    public function getTotalSqft() {
        return $this->get(self::TOTAL_SQFT);
    }

    public function getFurnishedStatus() {
        return $this->get(self::FURNISHED_STATUS);
    }

    public function getBalcony() {
        return $this->get(self::BALCONY);
    }

    public function getOverlooking() {
        return $this->get(self::OVERLOOKING);
    }

    public function getWaterAvailability() {
        return $this->get(self::WATER_AVAILABILITY);
    }

    public function getPowerBackup() {
        return $this->get(self::POWER_BACKUP);
    }

    public function getLandmark() {
        return $this->get(self::LANDMARK);
    }

    public function getParking() {
        return $this->get(self::PARKING);
    }

    public function getMaintenancePmo() {
        return $this->get(self::MAINTENANCE_PMO);
    }

    public function getConstructionAge() {
        return $this->get(self::CONSTRUCTION_AGE);
    }

    public function getReraId() {
        return $this->get(self::RERA_ID);
    }

    public function getExpectedPrice() {
        return $this->get(self::EXPECTED_PRICE);
    }

    public function getRegistryStatus() {
        return $this->get(self::REGISTRY_STATUS);
    }

    public function getRentalValue() {
        return $this->get(self::RENTAL_VALUE);
    }

    public function getDescription() {
        return $this->get(self::DESCRIPTION);
    }

    public function getExclusive() {
        return $this->get(self::EXCLUSIVE);
    }
}