<?php


namespace App\Api\V1\Requests;


use App\Services\Contracts\KnowledgeCenterUpdateContract;

class KnowledgeCenterUpdateRequest extends BaseRequest implements KnowledgeCenterUpdateContract {

    const CATEGORY      = 'category';
    const HEADLINE      = 'headline';
    const DESCRIPTION   = 'description';


    public function rules() {
        return[
            self::CATEGORY      => 'nullable|valid_kc_category_types',
            self::HEADLINE      => 'nullable|string',
            self::DESCRIPTION   => 'nullable|string',
        ];
    }

    public function hasCategory() {
        return $this->has(self::CATEGORY);
    }

    public function hasHeadline() {
        return $this->has(self::HEADLINE);
    }

    public function hasDescription() {
        return $this->has(self::DESCRIPTION);
    }

    public function getCategory() {
        return $this->get(self::CATEGORY);
    }

    public function getHeadline() {
        return $this->get(self::HEADLINE);
    }

    public function getDescription() {
        return $this->get(self::DESCRIPTION);
    }
}
