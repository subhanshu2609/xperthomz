<?php


namespace App\Api\V1\Requests;


use App\Services\Contracts\ServiceUpdateContract;

class ServiceUpdateRequest extends BaseRequest implements ServiceUpdateContract
{
    const HEADING = 'heading';
    const DESCRIPTION = 'description';

    public function rules()
    {
        return[
            self::HEADING => 'required|string',
            self::DESCRIPTION => 'required|string'
        ];
    }

    public function hasHeading(){
        return $this->has(self::HEADING);
    }

    public function getHeading(){
        return $this->get(self::HEADING);
    }

    public function hasDescription(){
        return $this->has(self::DESCRIPTION);
    }

    public function getDescription(){
        return $this->get(self::DESCRIPTION);
    }
}
