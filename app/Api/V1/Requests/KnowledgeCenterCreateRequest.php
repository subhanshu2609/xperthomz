<?php


namespace App\Api\V1\Requests;


use App\Services\Contracts\KnowledgeCenterCreateContract;


class KnowledgeCenterCreateRequest extends BaseRequest implements KnowledgeCenterCreateContract {
    const CATEGORY      = 'category';
    const HEADLINE      = 'headline';
    const DESCRIPTION   = 'description';


    public function rules() {
        return[
            self::CATEGORY      => 'required|valid_kc_category_types',
            self::HEADLINE      => 'required|string',
            self::DESCRIPTION   => 'required|string',
        ];
    }

    public function getCategory() {
        return $this->get(self::CATEGORY);
    }

    public function getHeadline() {
        return $this->get(self::HEADLINE);
    }

    public function getDescription() {
        return $this->get(self::DESCRIPTION);
    }
}
