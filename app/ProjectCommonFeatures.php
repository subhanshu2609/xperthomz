<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProjectCommonFeatures
 *
 * @property mixed $id
 * @property int $project_id
 * @property string $feature_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectCommonFeatures newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectCommonFeatures newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectCommonFeatures query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectCommonFeatures whereFeatureName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectCommonFeatures whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectCommonFeatures whereProjectId($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectCommonFeatures whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProjectCommonFeatures whereUpdatedAt($value)
 */
class ProjectCommonFeatures extends BaseModel
{
    //
}
