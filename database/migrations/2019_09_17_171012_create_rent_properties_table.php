<?php

use App\RentProperty;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent_properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->unsignedBigInteger('owner_id');
            $table->enum('property_type',[
                RentProperty::COMMERCIAL,
                RentProperty::RESIDENTIAL
            ]);
            $table->enum('rent_type', [
                RentProperty::RENT,
                RentProperty::PG
            ]);
            $table->boolean('featured_property');
            $table->enum('property_category', [
                    'apartment',
                    'flat',
                    'builder_floor',
                    'land',
                    'house',
                    'villa'
            ]);
            $table->string('address_1');
            $table->string('address_2');
            $table->enum('accommodation', [
                'one',
                'one_study',
                'two',
                'two_study',
                'three',
                'three_study',
                'four',
                'four_study',
            ]);
            $table->enum('facing', [
                'north',
                'east',
                'west',
                'south'
            ]);
            $table->double('total_sqft');
            $table->enum('furnished_status', [
                'fully_furnished',
                'semi_furnished',
                'unfurnished'
            ]);
            $table->integer('balcony');
            $table->enum('overlooking', [
                'park',
                'road',
                'other'
            ]);
            $table->boolean('water_availability');
            $table->boolean('power_backup');
            $table->string('landmark')->nullable();
            $table->boolean('parking');
            $table->double('maintenance_pmo');
            $table->integer('construction_age')->nullable();
            $table->double('expected_rent');
            $table->enum('availability', [
                'immediate',
                'specific_time'
            ]);
            $table->enum('tenant_type', [
                'family',
                'bachelor',
                'any'
            ])->nullable();
            $table->text('description')->nullable();
            $table->boolean('exclusive');
            $table->timestamps();

            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rent_properties');
    }
}
