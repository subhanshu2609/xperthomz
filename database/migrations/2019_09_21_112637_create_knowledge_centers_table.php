<?php

use App\KnowledgeCenter;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKnowledgeCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knowledge_centers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('category',[
                KnowledgeCenter::INDIAN,
                KnowledgeCenter::NRI
                ]);
            $table->string('headline');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knowledge_centers');
    }
}
