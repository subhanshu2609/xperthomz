<?php

use App\ProjectAccommodation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectAccommodationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_accommodations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('project_id');
            $table->enum('type',[
                'one',
                'one_study',
                'two',
                'two_study',
                'three',
                'three_study',
                'four',
                'four_study'
            ]);
            $table->bigInteger('price_per_sqft');
            $table->bigInteger('total_sqft');
            $table->enum('furnished_status',[
                ProjectAccommodation::UNFURNISHED,
                ProjectAccommodation::SEMI_FURNISHED,
                ProjectAccommodation::FULLY_FURNISHED,
            ]);
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_accommodations');
    }
}
