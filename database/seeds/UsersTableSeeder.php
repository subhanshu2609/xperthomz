<?php


use App\User;

class UsersTableSeeder extends DatabaseSeeder {
    public function run() {
        User::create([
            'first_name'    => 'Test',
            'last_name'     => 'Owner',
            'email'         => 'admin@test.com',
            'phone_number'  => '9999999999',
            'type'          => 'owner',
            'password'      => 'secret'
        ]);
        User::create([
            'first_name'    => 'Test',
            'last_name'     => 'Buyer',
            'email'         => 'buyer@test.com',
            'phone_number'  => '8888888888',
            'type'          => 'buyer',
            'password'      => 'secret'
        ]);
        User::create([
            'first_name'    => 'Test',
            'last_name'     => 'Seller',
            'email'         => 'seller@test.com',
            'phone_number'  => '7777777777',
            'type'          => 'seller',
            'password'      => 'secret'
        ]);
    }
}