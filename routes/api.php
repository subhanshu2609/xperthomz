<?php

$api = app('Dingo\Api\Routing\Router');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$basePath     = "App\Api\V1\Controllers\\";
$webhooksPath = $basePath . "Webhooks\\";

$api->version('v1', function ($api) use ($basePath, $webhooksPath) {
    $api->get('test', function () {
        return 'PUBLIC_ROUTE';
    });

    // Auth Related
    $api->post('authenticate', $basePath . 'AuthController@authenticate');
    $api->post('users', $basePath . 'UserController@create');



    // Webhooks
    $api->group(['prefix' => 'webhooks'], function ($api) use ($webhooksPath) {
        // AWS
        $api->group(['prefix' => 'aws'], function ($api) use ($webhooksPath) {
            //
        });
    });
});


// All Authenticated Routes Goes Here.
$api->version('v1', ['middleware' => 'api.custom-auth'], function ($api) use ($basePath) {

    // Auth Related
    $api->get('me', $basePath . 'UserController@showMe');
    $api->put('me', $basePath . 'UserController@updateMe');

    // User Related
    $api->get('users/{id}', $basePath . 'UserController@show');
    $api->get('users', $basePath . 'UserController@index');
    $api->put('users/{id}', $basePath . 'UserController@update');
    $api->delete('users/{id}', $basePath . 'UserController@delete');

    // Rent Property Related
    $api->get('rent-properties', $basePath . 'RentPropertyController@show');
    $api->get('rent-properties', $basePath . 'RentPropertyController@index');
    $api->post('rent-properties', $basePath . 'RentPropertyController@create');
    $api->put('rent-properties/{id}', $basePath . 'RentPropertyController@update');
    $api->delete('rent-properties/{id}', $basePath . 'RentPropertyController@delete');

    // Service
    $api->post('services', $basePath . 'ServiceController@create');
    $api->put('services/{id}', $basePath . 'ServiceController@update');
    $api->get('services', $basePath . 'ServiceController@index');
    $api->delete('services/{id}', $basePath . 'ServiceController@delete');

    // Project
    $api->get('projects', $basePath . 'ProjectController@index');
    $api->post('projects', $basePath . 'ProjectController@create');
    $api->put('projects/{id}', $basePath . 'ProjectController@update');
    $api->delete('projects/{id}', $basePath . 'ProjectController@delete');

    //KnowledgeCenter
    $api->get('knowledge-centers', $basePath . 'KnowledgeCenterController@index');
    $api->post('knowledge-centers', $basePath . 'KnowledgeCenterController@create');
    $api->put('knowledge-centers/{id}', $basePath . 'KnowledgeCenterController@update');
    $api->delete('knowledge-centers/{id}', $basePath . 'KnowledgeCenterController@delete');

    // Buy Property Related
    $api->get('buy-properties', $basePath . 'BuyPropertyController@show');
    $api->get('buy-properties', $basePath . 'BuyPropertyController@index');
    $api->post('buy-properties', $basePath . 'BuyPropertyController@create');
    $api->put('buy-properties/{id}', $basePath . 'BuyPropertyController@update');
    $api->delete('buy-properties/{id}', $basePath . 'BuyPropertyController@delete');

});
